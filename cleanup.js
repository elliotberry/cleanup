const rimraf = require("rimraf");
const freeSpace = require('@knutkirkhorn/free-space');
const prettyBytes = require('pretty-bytes');
 
freeSpace().then(bytes => {
    console.log('Free space: ' + prettyBytes(bytes));
   
});
const dirsToDelete = [
  {
    description: "Trash on all mounted volumes",
    paths: [`/Volumes/*/.Trashes/*`, "~/.Trash/*"],
  },
  {
    description: "System Log Files",
    paths: [
      `/private/var/log/asl/*.asl`,
      `/Library/Logs/DiagnosticReports/*`,
      `/Library/Logs/Adobe/*`,
      `~/Library/Containers/com.apple.mail/Data/Library/Logs/Mail/*`,
      `~/Library/Logs/CoreSimulator/*`,
    ],
  },
  {
    description: "ios dev misc",
    paths: [
      `~/Library/Developer/Xcode/iOS Device Logs/*`,
      `~/Music/iTunes/iTunes\ Media/Mobile\ Applications/*`,
      `~/Library/Application\ Support/MobileSync/Backup/*`,
    ],
  },
  {
    description: "pip cache",
    paths: [`~/Library/Caches/pip`],
  },
  {
    description: "XCode Derived Data and Archives",
    paths: [`~/Library/Developer/Xcode/DerivedData/*`],
  },
  {
    description: "adobe cache",
    paths: [
      `~/Library/Application\ Support/Adobe/Common/Media\ Cache\ Files/*`,
    ],
  },
];
/* misc other
if [ "$PYENV_VIRTUALENV_CACHE_PATH" ]; then
    echo 'Removing Pyenv-VirtualEnv Cache...'
    rm -rfv $PYENV_VIRTUALENV_CACHE_PATH &>/dev/null
fi

if type "npm" &> /dev/null; then
    echo 'Cleanup npm cache...'
    npm cache clean --force
fi

if type "yarn" &> /dev/null; then
    echo 'Cleanup Yarn Cache...'
    yarn cache clean --force
fi

echo 'Purge inactive memory...'
sudo purge
*/

function execShellCommand(cmd) {
  const exec = require("child_process").exec;
  return new Promise((res, rej) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        rej(error);
      } else if (stderr) {
        rej(stderr);
      } else {
        res(stdout);
      }
    });
  });
}

const main = async function () {
  for await (item of dirsToDelete) {
    console.log(`Removing ${item.paths.length} paths: ${item.description}`);
    for await (folder of item.paths) {
      try {
        await execShellCommand(`sudo rm -rf ${folder}`);
      } catch (e) {
        console.log(`Error removing ${folder}: ${e}`);
      }
    }
  }
};

//main();
